FROM python:3-alpine

RUN pip3 install speedtest-cli prometheus-client

COPY run-speedtest.py /

EXPOSE 9104
ENTRYPOINT ["/usr/local/bin/python3", "-u", "/run-speedtest.py"]
